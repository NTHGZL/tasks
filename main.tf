# Terraform configuration for elastic beanstalk environnement for golang application
#
#required variables:
#  - env_name: name of the environment
#  - app_name: name of the application
#  - app_version: version of the application
#  - region: region of the environment
#  - bucket_name: name of the bucket to store the application
#  - bucket_key: key of the bucket to store the application
#  - bucket_region: region of the bucket to store the application


# Create the environment

resource "aws_elastic_beanstalk_environment" "env" {
  name = "${var.env_name}"
  application = "${aws_elastic_beanstalk_application.app.name}"
  solution_stack_name = "64bit Amazon Linux 2016.03 v2.1.0 running Go 1.4"
  tier {
    name = "WebServer"
    type = "Standard"
    version = "64bit Amazon Linux 2016.03 v2.1.0 running Go 1.4"
  }
  version_label = "${var.app_version}"
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "ENVIRONMENTTYPE"
    value = "golang"
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "GO_ENV"
    value = "dev"
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "GO_REGION"
    value = "${var.region}"
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "GO_BUCKET_NAME"
    value = "${var.bucket_name}"
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "GO_BUCKET_KEY"
    value = "${var.bucket_key}"
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "GO_BUCKET_REGION"
    value = "${var.bucket_region}"
  }
}